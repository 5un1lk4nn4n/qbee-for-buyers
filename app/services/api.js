import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { MAIN_URL, URLS } from '../constants';

let token = null;
let head = null;
const auth = axios.create();

const bootstrapAsync = async () => {
  token = await AsyncStorage.getItem('@token');
  if (token) {
    head = { Authorization: `Bearer ${token}` };
  }
};

const getRequest = (url, callback) => {
  axios
    .get(url, {
      headers: {
        ...head,
        'content-type': 'application/json',
        // other header fields
      },
    })
    .then(response => {
      const { data } = response;
      callback(data);
    })
    .catch(error => {
      if (error.response && error.response.status === 404) {
        callback(error.response.data);
      }
    });
};

const postRequest = (url, callback, data) => {
  axios
    .post(url, data, {
      headers: {
        ...head,
        'content-type': 'application/json',
        // other header fields
      },
    })
    .then(response => {
      callback(response.data);
    })
    .catch(error => {
      if (
        error.response.status === 400 ||
        error.response.status === 401 ||
        error.response.status === 404
      ) {
        callback(error.response.data);
      }
    });
};

const putRequest = (url, callback, data) => {
  axios
    .put(url, data, {
      headers: {
        ...head,
        'content-type': 'application/json',
        // other header fields
      },
    })
    .then(response => {
      console.log(response);

      callback(response.data);
    })
    .catch(error => {
      console.log(error);
      if (
        error.response.status === 400 ||
        error.response.status === 401 ||
        error.response.status === 404
      ) {
        callback(error.response.data);
      }
    });
};

const deleteRequest = (url, callback) => {
  axios
    .delete(url, {
      headers: {
        ...head,
        'content-type': 'application/json',
        // other header fields
      },
    })
    .then(response => {
      callback(response.data);
    })
    .catch(error => {
      if (
        error.response.status === 400 ||
        error.response.status === 401 ||
        error.response.status === 404
      ) {
        callback(error.response.data);
      }
    });
};

export const login = (credential, callback) => {
  const url = `${MAIN_URL}${URLS.LOGIN}`;
  console.log('loging');
  auth
    .post(url, credential)
    .then(response => {
      callback(response.data);
    })
    .catch(error => {
      if (
        error.response.status === 400 ||
        error.response.status === 401 ||
        error.response.status === 404
      ) {
        callback(error.response.data);
      }
    });
};

export const otpVerify = (credential, callback) => {
  const url = `${MAIN_URL}${URLS.OTP_VERIFY}`;
  auth
    .post(url, credential)
    .then(response => {
      callback(response.data);
    })
    .catch(error => {
      if (
        error.response.status === 400 ||
        error.response.status === 401 ||
        error.response.status === 404
      ) {
        callback(error.response.data);
      }
    });
};

export const apiService = (method, url, callback, data) => {
  const getToken = () => {
    if (!token) {
      setTimeout(() => {
        getToken();
      }, 100);
    } else {
      switch (method) {
        case 'GET': {
          getRequest(url, callback, data);
          break;
        }
        case 'POST': {
          postRequest(url, callback, data);
          break;
        }
        case 'PUT': {
          putRequest(url, callback, data);
          break;
        }
        case 'DELETE': {
          deleteRequest(url, callback, data);
          break;
        }
        default:
          getRequest(url, callback, data);
          break;
      }
    }
  };
  bootstrapAsync();
  getToken();
};

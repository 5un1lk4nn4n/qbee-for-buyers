import React from 'react';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import AuthLoadingScreen from '../screens/AuthLoading';
import SignInScreen from '../screens/SignInScreen';
import OtpScreen from '../screens/OtpScreen';
import HomeScreen from '../screens/HomeScreen';
import AppIntroScreen from '../screens/AppIntroScreen';
import SupportScreen from '../screens/SupportScreen';
import PolicyScreen from '../screens/PolicyScreen';

const AuthStack = createStackNavigator({
  SignIn: SignInScreen,
  Otp: OtpScreen,
});

const ShopperStack = createStackNavigator({
  Home: HomeScreen,
  Support: SupportScreen,
  Policy: PolicyScreen,
  AppIntro: AppIntroScreen,
});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      Auth: AuthStack,
      App: ShopperStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);

import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { showMessage } from 'react-native-flash-message';
import { login as goLogin } from '../services/api';

import LogoCard from '../components/LogoCard';
import { Colors, Typography } from '../styles';
import Toast from '../utils/Toast';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.colorLight,
    alignItems: 'center',
  },

  textbox: {
    width: wp('80%'),
    borderBottomColor: Colors.colorDark,
    color: Colors.colorDark,
    textAlign: 'center',
    margin: hp('3%'),
    ...Typography.iosSecondary,
    ...Typography.fontMedium,
    height: hp('5%'),
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: hp('2%'),
  },
  nextBtn: {
    backgroundColor: Colors.colorAppSec,
    width: wp('40%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },

  btnLabel: {
    color: Colors.colorLight,
    alignSelf: 'center',
    ...Typography.iosTitle,
    ...Typography.fontMedium,
  },
  infoMsg: {
    color: Colors.colorLightDark,
    ...Typography.iosSecondary,
    margin: hp('2%'),
    ...Typography.fontLight,
    alignSelf: 'center',
  },
});

export default class SignInScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  message = null;

  companyRef = React.createRef(TextInput);

  mobileRef = React.createRef(TextInput);

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  componentDidMount() {
    this.isMounted = true;
  }

  showNext = () => {
    const { code, mobile } = this.state;
    if (!code && !mobile) {
      showMessage({
        message: 'Company code and mobile is required ',
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });
      return;
    }
    if (code.length !== 16) {
      showMessage({
        message: 'Invalid company code',
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });
      return;
    }
    if (!Number.isInteger(parseInt(mobile, 10)) || mobile.length !== 10) {
      showMessage({
        message: 'Invalid mobile',
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });

      return;
    }
    goLogin({ mobile, code }, this.login);
  };

  login = data => {
    if (data.code === 200) {
      const { navigation } = this.props;
      const { mobile, code } = this.state;
      this.showToast(data.message);
      navigation.navigate('Otp', {
        mobile,
        code,
      });
    } else if (data.code === 401) {
      showMessage({
        message: data.message,
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });
    }
  };

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  showToast = msg => {
    this.message = msg;
    this.setState(
      {
        visible: true,
      },
      () => {
        this.hideToast();
      },
    );
  };

  handleCompanyChange = val => {
    this.setState({
      code: val,
    });
  };

  handleMobileChange = val => {
    this.setState({
      mobile: val,
    });
  };

  renderLogin = () => {
    return (
      <View style={{ marginTop: hp('3%') }}>
        <TextInput
          onChangeText={val => this.handleCompanyChange(val)}
          placeholder="Your Company Code"
          autoCorrect={false}
          index={0}
          ref={this.companyRef}
          secureTextEntry={false}
          keyboardType="default"
          returnKeyType="next"
          style={styles.textbox}
          selectionColor={Colors.colorAppSec}
          placeholderTextColor={Colors.colorDark}
          onSubmitEditing={() => {
            if (this.companyRef.current) this.mobileRef.current.focus();
          }}
        />
        <TextInput
          onChangeText={val => this.handleMobileChange(val)}
          placeholder="Your Mobile"
          autoCorrect={false}
          index={1}
          ref={this.mobileRef}
          secureTextEntry={false}
          keyboardType="number-pad"
          returnKeyType="done"
          style={styles.textbox}
          selectionColor={Colors.colorAppSec}
          placeholderTextColor={Colors.colorDark}
          onSubmitEditing={() => {
            this.showNext();
          }}
        />
        <Text style={styles.infoMsg}>You will recieve OTP to login</Text>
      </View>
    );
  };

  render() {
    const { visible } = this.state;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <LogoCard />
        {this.renderLogin()}
        <TouchableOpacity style={styles.nextBtn} onPress={() => this.showNext()}>
          <Text style={styles.btnLabel}>Send Otp</Text>
        </TouchableOpacity>
        <Toast visible={visible} message={this.message} />
      </KeyboardAvoidingView>
    );
  }
}

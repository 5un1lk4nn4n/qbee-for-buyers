import React from 'react';

import { StyleSheet, Text, View, BackHandler } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import LottieView from 'lottie-react-native';

import Icon from 'react-native-vector-icons/Feather';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import qrscan from '../assets/animations/qrscan.json';
import addtocart from '../assets/animations/addtocart.json';
import love from '../assets/animations/love.json';
import money from '../assets/animations/money.json';
import group from '../assets/animations/group.json';

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    height: hp('80%'),
  },
  image: {
    width: wp('100%'),
    height: hp('30%'),
    marginBottom: hp('10%'),
  },
  text: {
    width: wp('70%'),
    marginTop: hp('2%'),
    color: '#00acee',
    textAlign: 'center',
    paddingHorizontal: 16,
    fontSize: 16,
  },
  title: {
    fontSize: 30,
    marginTop: hp('15%'),
    color: '#00acee',
    backgroundColor: 'transparent',
    textAlign: 'center',
    // marginBottom: 16,
    marginBottom: hp('2%'),
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#00acee',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const slides = [
  {
    key: '0',
    color: '#fff',
  },
  {
    key: '1',
    color: '#F8F8F8',
  },
  {
    key: '2',
    color: '#F8F8F8',
  },
  {
    key: '3',
    color: '#F8F8F8',
  },
];

export default class AppIntroScreen extends React.PureComponent {
  static navigationOptions = {
    header: null,
  };

  animation = [];

  async componentDidMount() {
    this.initAnimation();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  initAnimation = () => {
    if (!this.animation[0]) {
      setTimeout(() => {
        this.initAnimation();
      }, 100);
    } else {
      this.animation[0].play();
      setTimeout(() => {
        this.animation[1].play();
      }, 3000);
      this.animation[2].play();
      this.animation[3].play();
    }
  };

  onDone = () => {
    const { navigation } = this.props;
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    // this.setState({ showGetStart: true });
    navigation.navigate('Home');
  };

  handleBackButton = () => {
    return true;
  };

  renderFirst = item => {
    return (
      <View style={[styles.mainContent, { backgroundColor: item.color }]}>
        <View style={{ flexDirection: 'row' }}>
          <LottieView
            ref={animation => {
              this.animation[0] = animation;
            }}
            style={{
              width: wp('40%'),
              height: wp('40%'),
              alignSelf: 'center',
            }}
            loop={false}
            source={qrscan}
            animation
          />
          <LottieView
            ref={animation => {
              this.animation[1] = animation;
            }}
            style={{
              width: wp('40%'),
              height: wp('40%'),
              alignSelf: 'center',
            }}
            loop={false}
            source={addtocart}
          />
        </View>
        <Text style={styles.title}>Start Shopping</Text>

        <Text style={styles.text}>Start shopping by scanning QR code of the store</Text>
        <Text style={styles.text}>Again Scan Barcode of your items and add to cart</Text>
        <Text style={styles.text}>
          Once done, pay using Amazon pay, Google pay, or card/ netbanking
        </Text>
        <Text style={styles.text}>No queue or waiting</Text>
      </View>
    );
  };

  renderSecond = item => {
    return (
      <View style={[styles.mainContent, { backgroundColor: item.color }]}>
        <LottieView
          ref={animation => {
            this.animation[2] = animation;
          }}
          style={{
            width: wp('50%'),
            height: wp('50%'),
            alignSelf: 'center',
          }}
          loop
          source={group}
        />

        <Text style={styles.title}>Group shopping</Text>
        <Text style={styles.text}>
          Share your cart with your friends by mobile number and enjoy group shopping!
        </Text>
      </View>
    );
  };

  renderFourth = item => {
    return (
      <View style={[styles.mainContent, { backgroundColor: item.color }]}>
        <LottieView
          ref={animation => {
            this.animation[3] = animation;
          }}
          style={{
            width: wp('50%'),
            height: wp('50%'),
            alignSelf: 'center',
          }}
          loop
          source={love}
        />
        <Text style={styles.title}>Made with love</Text>
        <Text style={styles.text}>
          Happy shopping! A lot more new features every month to make your shopping experiance
          great!
        </Text>
      </View>
    );
  };

  renderThird = item => {
    return (
      <View style={[styles.mainContent, { backgroundColor: item.color }]}>
        <LottieView
          ref={animation => {
            this.animation[4] = animation;
          }}
          style={{
            width: wp('100%'),
            height: wp('50%'),
            alignSelf: 'center',
          }}
          loop
          source={money}
        />
        <Text style={styles.title}>Enjoy lot of offers</Text>
        <Text style={styles.text}>
          Know all the available offers of the store before you start shopping
        </Text>
      </View>
    );
  };

  renderItem = item => {
    if (item.key === '0') {
      return this.renderFirst(item);
    }
    if (item.key === '1') {
      return this.renderSecond(item);
    }
    if (item.key === '2') {
      return this.renderThird(item);
    }
    if (item.key === '3') {
      return this.renderFourth(item);
    }
    return null;
  };

  renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="arrow-right"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  };

  renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="check"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppIntroSlider
          slides={slides}
          renderItem={this.renderItem}
          dotStyle={{ backgroundColor: '5c5c5c' }}
          activeDotStyle={{ backgroundColor: '#00acee' }}
          renderDoneButton={this.renderDoneButton}
          renderNextButton={this.renderNextButton}
          onDone={this.onDone}
        />
      </View>
    );
  }
}

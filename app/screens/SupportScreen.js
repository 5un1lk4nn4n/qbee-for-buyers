import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Clipboard, Linking } from 'react-native';

import LottieView from 'lottie-react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-ionicons';
import { Colors, Typography } from '../styles';

import support from '../assets/animations/rocket.json';
import Toast from '../components/Toast';

const whatsapp = require('../assets/images/whatsup.png');

const styles = StyleSheet.create({
  home: {
    flex: 1,
  },
});
export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  message = null;

  state = {
    visible: false,
  };

  async componentDidMount() {
    this.initAnimation();
  }

  initAnimation = () => {
    if (!this.animation) {
      setTimeout(() => {
        this.initAnimation();
      }, 100);
    } else {
      this.animation.play();
    }
  };

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  showToast = msg => {
    this.message = msg;
    this.setState({ visible: true }, () => {
      this.hideToast();
    });
  };

  render() {
    const { visible } = this.state;
    return (
      <View style={styles.home}>
        <LottieView
          ref={animation => {
            this.animation = animation;
          }}
          style={{
            marginTop: hp('5%'),
            marginBottom: hp('8%'),
            width: wp('50%'),
            height: wp('50%'),
            alignSelf: 'center',
          }}
          loop
          source={support}
        />
        <View
          style={{
            justifyContent: 'space-between',
            alignItems: 'center',
            alignSelf: 'center',
            height: hp('15%'),
            width: wp('90%'),
          }}
        >
          <Text
            style={{
              color: Colors.colorDark,
              fontSize: 18,
              borderRadius: 20,
...Typography.fontMedium
            }}
          >
            For any feedback and support, click on whatsapp below (If you are added our number
            already else copy our whatsapp number below)
          </Text>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 20,
            }}
            onPress={() => Linking.openURL('whatsapp://send?text=Hey Qbee,&phone=+919442640132')}
          >
            <Image source={whatsapp} style={{ width: wp('10%'), height: wp('10%') }} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            alignSelf: 'center',
            justifyContent: 'space-between',
            height: hp('20%'),
            marginLeft: wp('5%'),
          }}
        >
          <Text style={{ ...Typography.fontMedium, ...Typography.iosSecondary }}>About US</Text>
          <Text style={{ ...Typography.fontLight }}>QbeeColony</Text>
          <Text style={{ ...Typography.fontLight }}>
            We are here to enrich your shopping experiance
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ ...Typography.fontLight }}>Email:support@qbeecolony.com</Text>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
              }}
              onPress={() => {
                Clipboard.setString('support@qbeecolony.com');
                this.showToast('Copied');
              }}
            >
              <Icon
                name="ios-copy"
                color={Colors.colorDark}
                size={wp('4%')}
                style={{ marginLeft: wp('2%') }}
              />
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ ...Typography.fontLight }}>Whatsapp:919442640132</Text>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
              }}
              onPress={() => {
                Clipboard.setString('+919442640132');
                this.showToast('Copied');
              }}
            >
              <Icon
                name="ios-copy"
                color={Colors.colorDark}
                size={wp('4%')}
                style={{ marginLeft: wp('2%') }}
              />
            </TouchableOpacity>
          </View>
          <Text style={{ ...Typography.fontLight }}>
            48, Chellandiamman nagar Singanallur, Coimbatore.(HO)
          </Text>
          <Text style={{ ...Typography.fontLight }}>For more details, visit</Text>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
            }}
            onPress={() => Linking.openURL('https://www.google.com')}
          >
            <Text style={{ ...Typography.fontLight }}>www.qbeecolony.com</Text>
          </TouchableOpacity>
        </View>
        <Toast visible={visible} message={this.message} />
      </View>
    );
  }
}

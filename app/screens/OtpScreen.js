import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, ToastAndroid } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import OtpInputs from 'react-native-otp-inputs';
import CountDown from 'react-native-countdown-component';
import timer from 'react-native-timer';
import { Bubbles } from 'react-native-loader';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from '../utils/Toast';

import LogoCard from '../components/LogoCard';

import { otpVerify } from '../services/api';
import { Typography, Colors } from '../styles';

export default class OtpScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      isCounter: true,
      loading: false,
      visible: false,
      message: null,
    };
  }

  resend = () => {
    const { isCounter } = this.state;
    if (isCounter) {
    } else {
      this.setState({
        isCounter: false,
      });
    }
  };

  showNext = () => {
    if (this.otp && this.otp.length == 4) {
      this.setState({
        loading: true,
      });
      const { navigation } = this.props;
      const mobile = navigation.getParam('mobile', null);
      const code = navigation.getParam('code', null);
      otpVerify({ mobile, otp: this.otp, code }, this.verify);
    }
  };

  verify = data => {
    if (data.code === 401) {
      this.showToast(data.message);
      this.setState({
        loading: false,
      });
    }
    if (data.code === 200) {
      this.storeData(data.data.token, data.data.name);
    }
  };

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  showToast = msg => {
    this.setState(
      {
        visible: true,
        message: msg,
      },
      () => {
        this.hideToast();
      },
    );
  };

  activeResend = () => {
    timer.setTimeout(
      'counter',
      () => {
        this.setState({
          isCounter: !this.state.isCounter,
        });
      },
      300,
    );
  };

  storeData = async (token, name) => {
    try {
      await AsyncStorage.setItem('@token', token);
      await AsyncStorage.setItem('@name', name);
    } catch (e) {
      // saving error
    }
    this.props.navigation.navigate('Home');
  };

  render() {
    const resendStyle = this.state.isCounter
      ? [styles.resendStyle, styles.resendStyleTextDimmed]
      : [styles.resendStyle, styles.resendStyleTextActive];
    return (
      <View>
        <LogoCard />
        <OtpInputs
          handleChange={code => (this.otp = code)}
          numberOfInputs={4}
          containerStyles={{ marginTop: 30, width: wp('80%'), alignSelf: 'center' }}
        />
        <View
          style={{
            flexDirection: 'row',
            width: wp('80%'),
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginTop: hp('12%'),
          }}
        >
          <Text
            style={{
              ...Typography.iosTertiary,
              ...Typography.fontLight,
              color: Colors.colorLightDark,
            }}
          >
            If you didn't recieve an OTP!
          </Text>
          <TouchableOpacity>
            <Text style={resendStyle}>Resend</Text>
          </TouchableOpacity>
          {this.state.isCounter ? (
            <>
              <Text> in </Text>
              <CountDown
                until={90}
                size={10}
                onFinish={this.activeResend.bind(this)}
                digitStyle={{ backgroundColor: '#FFF', padding: 0 }}
                digitTxtStyle={{ color: '#1CC625' }}
                timeToShow={['M', 'S']}
                timeLabels={{ m: null, s: null }}
                showSeparator
              />
            </>
          ) : null}
        </View>

        <TouchableOpacity style={styles.nextBtn} onPress={this.showNext.bind(this)}>
          {this.state.loading ? (
            <Bubbles color={Colors.colorLight} size={4} />
          ) : (
            <Text style={styles.btnLabel}>Next</Text>
          )}
        </TouchableOpacity>
        <Toast visible={this.state.visible} message={this.state.message} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  resendStyle: {
    ...Typography.fontMedium,
    ...Typography.iosTertiary,
  },
  resendStyleTextDimmed: {
    color: Colors.colorLightDark,
  },
  resendStyleTextActive: {
    color: Colors.colorAppSec,
  },
  nextBtn: {
    backgroundColor: Colors.colorAppSec,
    width: wp('40%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp('6%'),
  },
  btnLabel: {
    color: Colors.colorLight,
    alignSelf: 'center',
    ...Typography.iosInputs,
    ...Typography.fontMedium,
  },
});

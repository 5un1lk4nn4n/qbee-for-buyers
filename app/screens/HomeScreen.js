import React from 'react';
import { View } from 'react-native';
// import Header from '../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import BubbleHeader from '../components/BubbleHeader';
import AddNew from '../components/AddNew';
import ShopperCart from '../components/ShopperCart';
import PopupMenu from '../components/PopupMenu';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeUser: null,
    };
  }

  addNewCb = newShop => {
    const { userList } = this.state;
    const userArr = [...userList];
    userArr.push(newShop);
    this.setState({
      userList: userArr,
    });
  };

  activeBubble = user => {
    console.log(user);
    if (user) {
      this.setState({
        activeUser: user,
      });
    } else {
      this.setState({
        activeUser: null,
      });
    }
  };

  render() {
    const { activeUser } = this.state;
    const { navigation } = this.props;
    return (
      <View style={{ justifyContent: 'flex-start' }}>
        {/* <Header navigation={navigation} /> */}
        <View
          style={{
            flexDirection: 'row-reverse',
            marginLeft: wp('5%'),
            marginTop: hp('3%'),
          }}
        >
          <PopupMenu navigation={navigation} />
        </View>
        <BubbleHeader callback={this.activeBubble} />
        <View>
          {!activeUser ? <AddNew callback={this.addNewCb} /> : <ShopperCart active={activeUser} />}
        </View>
      </View>
    );
  }
}

import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import { DotsLoader } from 'react-native-indicator';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { showMessage } from 'react-native-flash-message';
import { URLS, MAIN_URL } from '../constants';
import { apiService } from '../services/api';
import { Typography, Colors } from '../styles';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // height: hp('100%'),
  },
  buttonText: {
    ...Typography.fontMedium,
    ...Typography.iosTitle,
    color: Colors.colorLight,
  },
  addBtn: {
    ...Colors.backgroundAppSec,
    width: wp('35%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    width: wp('80%'),
    borderColor: Colors.colorDark,
    borderWidth: 1,
    height: hp('6%'),
    marginBottom: hp('3%'),
    textAlign: 'center',
    borderRadius: 40,
    ...Typography.iosTitle,
    ...Typography.fontLight,
  },
});

export default class AddNew extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      mobile: null,
      loading: false,
    };
  }

  addNew = () => {
    const { mobile, loading } = this.state;
    if (loading) {
      showMessage({
        message: 'Connecting to shopper. please wait!',
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });
      return;
    }
    if (mobile && mobile.length === 10 && Number.isInteger(parseInt(mobile, 10))) {
      const url = `${MAIN_URL}${URLS.CONNECT}`;
      const postData = {
        mobile,
      };
      this.setState({ loading: true });
      apiService('POST', url, this.addShopper, postData);
    } else {
      showMessage({
        message: 'Invalid mobile number',
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });
    }
    // this.props.callback(NEW)
  };

  addShopper = data => {
    const { callback } = this.props;
    if (data.code === 400) {
      showMessage({
        message: data.error,
        type: 'default',
        backgroundColor: Colors.colorAppSec, // background color
        color: Colors.colorLight,
      });
    } else if (data.code === 200) {
      callback(data.data);
    }
    this.setState({ loading: false });
  };

  handleMobileChange = val => {
    this.setState({
      mobile: val,
    });
  };

  render() {
    const { loading } = this.state;
    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardVerticalOffset={hp('5%')}
        behavior="padding"
        enabled
      >
        {/* <ImageBackground
          source={require('../assets/images/bg.jpg')}
          style={{ width: wp('100%'), height: hp('100%') }}
          resizeMode="cover"
        > */}
        <Text
          style={{
            fontSize: 30,
            ...Typography.fontBold,
            width: wp('90%'),
            marginLeft: wp('5%'),
            marginBottom: hp('1%'),
            // marginTop: wp('5%'),
          }}
        >
          Connect
        </Text>
        <Text
          style={{
            fontSize: 25,
            ...Typography.fontBold,
            width: wp('90%'),
            marginBottom: hp('2%'),
            marginLeft: wp('5%'),
          }}
        >
          to your Customer's Cart now!
        </Text>
        <Text
          style={{
            fontSize: 16,
            ...Typography.fontMedium,
            marginBottom: hp('2%'),
            width: wp('80%'),
            alignSelf: 'center',
          }}
        >
          Here's how it works
        </Text>
        <Text
          style={{
            fontSize: 14,
            ...Typography.fontMedium,
            marginBottom: hp('2%'),
            width: wp('80%'),
            alignSelf: 'center',
          }}
        >
          1) Connect to your customer's shopping cart
        </Text>
        <Text
          style={{
            fontSize: 14,
            ...Typography.fontMedium,
            marginBottom: hp('2%'),
            width: wp('80%'),
            alignSelf: 'center',
          }}
        >
          2) View their items, shopping status, payment status
        </Text>
        <View style={{ marginTop: hp('3%'), alignItems: 'center' }}>
          <TextInput
            onChangeText={val => this.handleMobileChange(val)}
            placeholder="Customer mobile number"
            autoCorrect={false}
            secureTextEntry={false}
            keyboardType="number-pad"
            returnKeyType="done"
            maxLength={10}
            style={styles.textbox}
            selectionColor={Colors.colorAppSec}
            placeholderTextColor={Colors.colorDark}
            onSubmitEditing={() => {
              this.addNew();
            }}
          />

          <TouchableOpacity style={styles.addBtn} onPress={() => this.addNew()}>
            {loading ? (
              <DotsLoader color={Colors.colorLight} size={10} frequency={200} />
            ) : (
              <Text style={styles.buttonText}>Add</Text>
            )}
          </TouchableOpacity>
        </View>
        {/* </ImageBackground> */}
      </KeyboardAvoidingView>
    );
  }
}

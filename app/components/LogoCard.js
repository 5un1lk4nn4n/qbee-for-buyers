import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import LottieView from 'lottie-react-native';
import { Typography, Colors } from '../styles';

const astronaut = require('../assets/animations/astronaut.json');

// const logo = require('../assets/images/logo.png');

const styles = StyleSheet.create({
  logo: {
    width: wp('60%'),
    height: wp('60%'),
  },
  title: {
    ...Typography.fontBold,
    fontSize: 30,
    color: Colors.colorDark,
  },
  tagline: {
    ...Typography.fontLight,
    ...Typography.iosTitle,
    color: Colors.colorDark,
  },
  logoBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
});

export default class LogoBox extends React.Component {
  async componentDidMount() {
    this.animation.play();
  }

  render() {
    return (
      <View style={[styles.logoBox, { marginTop: hp('10%') }]}>
        {/* <Image style={styles.logo} source={logo} /> */}
        <LottieView
          ref={animation => {
            this.animation = animation;
          }}
          style={{
            width: wp('50%'),
            height: wp('50%'),
          }}
          loop
          source={astronaut}
        />
        <Text style={styles.title}>Qbee</Text>
        <Text style={styles.tagline}>Great shopping experiance!</Text>
        {/* <Text style={styles.tagline}>No Queue.. No Waitng..</Text> */}
      </View>
    );
  }
}

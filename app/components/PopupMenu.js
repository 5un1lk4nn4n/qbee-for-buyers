import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-ionicons';
import AsyncStorage from '@react-native-community/async-storage';

import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { Typography, Colors } from '../styles';

const styles = StyleSheet.create({
  title: {
    ...Typography.fontMedium,
    ...Typography.iosTitle,
    color: Colors.colorLight,
    marginLeft: 20,
  },
  menuText: {
    ...Typography.fontMedium,
    ...Typography.iosTertiary,
  },
  divider: {
    marginVertical: 5,
    marginHorizontal: 2,
    borderBottomWidth: 1,
    borderColor: '#ccc',
  },
});

const optionsStyles = {
  optionsContainer: {
    backgroundColor: '#fff',
    padding: 5,
    width: wp('70%'),
    height: hp('34%'),
    borderRadius: 20,
  },
  //   optionsWrapper: {
  //     backgroundColor: 'purple',
  //   },
  optionWrapper: {
    margin: 5,
  },
  //   optionTouchable: {
  //     underlayColor: 'gold',
  //     activeOpacity: 70,
  //   },
  //   optionText: {
  //     color: 'brown',
  //   },
};
let name = null;

const exitApp = props => {
  AsyncStorage.removeItem('@token');
  AsyncStorage.removeItem('@name');
  props.navigation.navigate('SignIn');
};

const getUsername = async () => {
  name = await AsyncStorage.getItem('@name');
  if (name) {
    name = name.charAt(0).toUpperCase() + name.slice(1);
  }
};
getUsername();

const showAppIntro = props => {
  props.navigation.navigate('AppIntro');
};

const showSupport = props => {
  props.navigation.navigate('Support');
};

const showPolicy = props => {
  props.navigation.navigate('Policy');
};
function PopupMenu(props) {
  return (
    <View>
      <Menu>
        <MenuTrigger>
          <Icon name="ios-options" size={25} color={Colors.colorAppSec} />
        </MenuTrigger>
        <MenuOptions customStyles={optionsStyles}>
          <MenuOption disabled>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <Image
                source={require('../assets/images/avatar/gm.jpg')}
                style={{ width: 35, height: 35, borderRadius: 50 }}
              />
              <Text
                style={{
                  color: Colors.colorDark,
                  ...Typography.fontMedium,
                  ...Typography.iosSecondary,
                  marginLeft: 5,
                }}
              >
                {name}
              </Text>
            </View>
          </MenuOption>
          <MenuOption onSelect={() => showAppIntro(props)}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <Icon name="ios-snow" size={25} color={Colors.colorDark} />
              <Text
                style={{
                  color: Colors.colorDark,
                  ...Typography.fontMedium,
                  ...Typography.iosTertiary,
                  marginLeft: 8,
                }}
              >
                App Intro
              </Text>
            </View>
          </MenuOption>
          <MenuOption onSelect={() => showSupport(props)}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <Icon name="ios-rocket" size={25} color={Colors.colorDark} />
              <Text
                style={{
                  color: Colors.colorDark,
                  ...Typography.fontMedium,
                  ...Typography.iosTertiary,
                  marginLeft: 8,
                }}
              >
                Help & Support
              </Text>
            </View>
          </MenuOption>

          <MenuOption onSelect={() => showPolicy(props)}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <Icon name="ios-flame" size={25} color={Colors.colorDark} />
              <Text
                style={{
                  color: Colors.colorDark,
                  ...Typography.fontMedium,
                  ...Typography.iosTertiary,
                  marginLeft: 8,
                }}
              >
                Our Privacy Policy
              </Text>
            </View>
          </MenuOption>
          <View style={styles.divider} />
          <MenuOption onSelect={() => exitApp(props)}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <Icon name="ios-log-out" size={25} color={Colors.colorDark} />
              <Text
                style={{
                  color: Colors.colorDark,
                  ...Typography.fontMedium,
                  ...Typography.iosTertiary,
                  marginLeft: 8,
                }}
              >
                Exit
              </Text>
            </View>
          </MenuOption>
        </MenuOptions>
      </Menu>
    </View>
  );
}
export default PopupMenu;

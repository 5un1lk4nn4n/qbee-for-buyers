import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import Icon from 'react-native-ionicons';
import AsyncStorage from '@react-native-community/async-storage';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import PopupMenu from './PopupMenu';
import { Typography, Colors } from '../styles';

const styles = StyleSheet.create({
  header: {
    height: hp('9%'),
    ...Colors.backgroundApp,
    ...Typography.androidSecondary,
    ...Typography.fontLight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.colorLighter,
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    ...Typography.fontMedium,
    ...Typography.iosTitle,
    color: Colors.colorLight,
  },
  menuText: {
    ...Typography.fontLight,
    ...Typography.iosTertiary,
  },
});

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
    };
    this.getUsername();
  }

  getUsername = async () => {
    const name = await AsyncStorage.getItem('@name');
    if (name) {
      this.setState({
        name: name.charAt(0).toUpperCase() + name.slice(1),
      });
    }
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.header}>
        <Text style={styles.title}>{`Hello ${this.state.name}!`}</Text>
        <PopupMenu navigation={navigation} />
      </View>
    );
  }
}

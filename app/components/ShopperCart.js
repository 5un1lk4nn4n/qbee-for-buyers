import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import timer from 'react-native-timer';
import LottieView from 'lottie-react-native';

import { Typography, Colors, Spacing } from '../styles';
import paid from '../assets/animations/success.json';
import shop from '../assets/animations/shop.json';
import bike from '../assets/animations/bike.json';
import { URLS, MAIN_URL } from '../constants';
import { apiService } from '../services/api';

const emptyCart = require('../assets/images/empty-cart.png');

const IN_SHOPPING = {
  width: wp('10%'),
  height: wp('10%'),
};
const OUT_SHOPPING = {
  width: wp('15%'),
  height: wp('15%'),
};

const styles = StyleSheet.create({
  container: {
    height: hp('100%'),
    ...Colors.backgroundWhite,
  },
  infoBox: {
    alignSelf: 'center',
    marginTop: hp('2%'),
  },
  priceBox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    ...Colors.backgroundWhite,
    ...Spacing.bordered,
    height: hp('10%'),
    margin: hp('2%'),
    borderWidth: 1,
    borderColor: Colors.colorLighter,
  },
  cartContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  title: {
    ...Typography.fontMedium,
    ...Typography.iosTitle,
  },
  titleBox: {
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: Colors.colorLighter,
    margin: hp('2%'),
    height: hp('3%'),
    textAlign: 'center',
  },

  rupSign: {
    ...Typography.fontMedium,
    ...Typography.iosSmall,
  },
  subTitle: {
    ...Typography.fontMedium,
    ...Typography.iosSecondary,
  },
  countNo: {
    ...Typography.fontMedium,
    ...Colors.backgroundAppSec,
    color: Colors.colorLight,
    borderRadius: 50,
    width: wp('10%'),
    height: wp('10%'),
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  mainPriceColor: {
    color: Colors.colorRed,
  },
  card: {
    flexDirection: 'row',
    width: wp('90%'),
    borderRadius: 10,
    alignItems: 'center',
    borderColor: Colors.colorLighter,
    borderWidth: 1,
    margin: 2,
  },
  counter: {
    ...Colors.backgroundAppSec,
    color: Colors.colorLight,
    width: wp('14%'),
    height: wp('14%'),
    textAlign: 'center',
    textAlignVertical: 'center',
    ...Typography.iosSecondary,
    ...Typography.fontLight,
    ...Spacing.LeftBordered,
  },
  itemDetail: {
    flexWrap: 'wrap',
    width: wp('70%'),
    paddingLeft: 6,
  },
  itemTextSub: {
    color: Colors.colorLightDark,
    marginTop: 4,
  },
  itemText: {
    color: Colors.colorDark,
    ...Typography.fontLight,
    ...Typography.iosTertiary,
  },
  price: {
    width: wp('16%'),
    ...Colors.backgroundAppSec,
    height: wp('14%'),
    textAlignVertical: 'center',
    lineHeight: wp('14%'),
    textAlign: 'center',
    position: 'absolute',
    right: 0,
    ...Spacing.RightBordered,
    color: Colors.colorLight,
  },
  money: {
    flexDirection: 'row',
  },
});

export default class ShopperCart extends React.Component {
  isMount = false;

  constructor(props) {
    super(props);
    const { active } = this.props;
    this.state = {
      user: active,
      loaded: false,
      shopping: null,
    };
  }

  componentDidMount() {
    this.isMount = true;
    this.getShopping();
    this.loader.play();
  }

  componentWillUnmount() {
    timer.clearInterval(this);
    this.isMount = false;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.active !== prevState.user) {
      return { user: nextProps.active, loaded: false };
    }
    return null;
  }

  initAnimation = () => {
    if (!this.animation) {
      setTimeout(() => {
        this.initAnimation();
      }, 100);
    } else {
      this.animation.play();
    }
  };

  getShopping = () => {
    timer.setInterval(
      this,
      'sync',
      () => {
        const { user } = this.state;
        const url = `${MAIN_URL}${URLS.SYNC_SHOPPER}?shopping_id=${user.shopping}`;
        apiService('GET', url, this.renderSync);
      },
      2000,
    );
  };

  renderSync = data => {
    if (data.code === 200 && this.isMount) {
      this.setState({
        shopping: data.data,
        loaded: true,
      });
    }
  };

  renderPriceBox = shopping => {
    const animationStyle = shopping.is_shopping ? IN_SHOPPING : OUT_SHOPPING;
    return (
      <>
        <View style={styles.money}>
          <Text style={styles.rupSign}>{'\u20B9'}</Text>
          <Text style={[styles.subTitle, styles.mainPriceColor]}>
            {shopping.is_shopping ? shopping.total_amount : shopping.grand_total}
          </Text>
        </View>
        <View>
          <LottieView
            ref={animation => {
              this.animation = animation;
              this.initAnimation();
            }}
            style={animationStyle}
            loop
            source={shopping.is_shopping ? shop : paid}
          />
        </View>
        <View>
          <Text style={styles.countNo}>{shopping.total_quantity}</Text>
        </View>
      </>
    );
  };

  renderCartList = item => {
    const data = item.item;
    return (
      <View style={styles.card}>
        <Text style={styles.counter}>{data.quantity}</Text>
        <View style={styles.itemDetail}>
          <Text style={styles.itemText}>{data.name}</Text>
          <Text style={[styles.itemText, styles.itemTextSub]}>
            {`\u20B9`}
            {data.price}
          </Text>
        </View>
        <Text style={[styles.itemText, styles.price]}>
          {'\u20B9'}
          {(data.quantity * data.price).toFixed(2)}
        </Text>
      </View>
    );
  };

  renderCart = cart => {
    if (!cart.length) {
      return (
        <View style={{ justifyContent: 'center', marginTop: 150 }}>
          <Image source={emptyCart} style={{ width: 100, height: 100, alignSelf: 'center' }} />
        </View>
      );
    }
    return (
      <ScrollView contentContainerStyle={styles.cartContainer}>
        <FlatList
          data={cart}
          renderItem={this.renderCartList}
          keyExtractor={(item, index) => index.toString()}
          extraData={cart}
        />
      </ScrollView>
    );
  };

  showLoader = () => {
    return (
      <View style={{ position: 'absolute', top: hp('25%'), right: wp('35%') }}>
        <LottieView
          ref={animation => {
            this.loader = animation;
          }}
          style={{ width: 100, height: 100 }}
          loop
          source={bike}
        />
      </View>
    );
  };

  render() {
    const { add, user, shopping, loaded } = this.state;
    if (!loaded) {
      return this.showLoader();
    }
    return (
      <View style={styles.container}>
        <View style={styles.infoBox}>
          <Text style={styles.title}>{user.name}</Text>
          <Text style={styles.title}>{user.shopping}</Text>
        </View>
        <View style={styles.priceBox}>{this.renderPriceBox(shopping)}</View>
        <Text style={[styles.title, styles.titleBox]}>Shopper's Cart</Text>
        {this.renderCart(shopping.cart)}
      </View>
    );
  }
}

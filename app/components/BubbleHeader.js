import React from 'react';
import { StyleSheet, FlatList, ScrollView, View } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import timer from 'react-native-timer';
import LottieView from 'lottie-react-native';

import { URLS, MAIN_URL } from '../constants';
import { apiService } from '../services/api';
import Bubble from './Bubble';
import { Colors } from '../styles';
import bike from '../assets/animations/bike.json';

const styles = StyleSheet.create({
  bubbleList: {
    // backgroundColor: 'green',
    height: hp('9%'),
    width: wp('100%'),
    alignItems: 'center',
    zIndex: 2,
  },
});
export default class BubbleHeader extends React.Component {
  isMount = false;

  watthefuck = false;

  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      loading: true,
    };
  }

  componentDidMount() {
    this.isMount = true;
    timer.setInterval(
      this,
      'bubble',
      () => {
        const url = `${MAIN_URL}${URLS.CONNECTED_LIST}`;
        apiService('GET', url, this.getActiveBubble);
      },
      3000,
    );
  }

  componentWillUnmount() {
    this.isMount = false;
    timer.clearInterval('bubble');
  }

  initAnimation = () => {
    if (!this.animation) {
      setTimeout(() => {
        this.initAnimation();
      }, 100);
    } else {
      this.animation.play();
    }
  };

  getActiveBubble = data => {
    if (this.isMount) {
      if (data.code === 200 && data.data.length) {
        this.setState({
          userList: data.data,
          loading: false,
        });
      } else {
        this.setState({
          loading: false,
        });
      }
    }
  };

  renderBubble = item => {
    const { callback } = this.props;
    return <Bubble callback={callback} item={item} />;
  };

  renderLoader = () => {
    return (
      <LottieView
        ref={animation => {
          this.animation = animation;
          this.initAnimation();
        }}
        style={{ width: wp('10%'), height: wp('10%') }}
        loop
        source={bike}
      />
    );
  };

  render() {
    const { userList, loading } = this.state;
    const { callback } = this.props;
    return (
      <ScrollView contentContainerStyle={styles.bubbleList} horizontal>
        <Bubble callback={callback} />
        {loading ? (
          <View style={{ marginLeft: 20 }}>{this.renderLoader()}</View>
        ) : (
          <FlatList
            horizontal
            data={userList}
            renderItem={this.renderBubble}
            keyExtractor={(item, index) => index.toString()}
            extraData={userList}
          />
        )}
      </ScrollView>
    );
  }
}

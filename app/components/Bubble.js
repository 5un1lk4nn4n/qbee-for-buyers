import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-ionicons';
import { Typography, Colors } from '../styles';

const styles = StyleSheet.create({
  dp: {
    ...Typography.fontMedium,
    ...Typography.iosTitle,
    color: Colors.colorLight,
  },
  bubble: {
    width: wp('14%'),
    height: wp('14%'),
    borderRadius: 50,
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bubbleActive: {
    ...Colors.backgroundAppSec,
  },
  bubbleInactive: {
    ...Colors.backgroundGrey,
  },
  checked: {
    position: 'absolute',
    top: -10,
    zIndex: 10,
  },
});
export default class Bubble extends React.PureComponent {
  onPress = (callback, user) => {
    callback(user);
  };

  render() {
    const { item, callback } = this.props;

    const user = item ? item.item : null;

    const bubbleStyle = [styles.bubble];
    const bubbleTextStyle = [styles.dp];

    if (user && user.is_paid) {
      bubbleStyle.push(styles.bubbleInactive);
      bubbleTextStyle.push({ color: Colors.colorDark });
    } else {
      bubbleStyle.push(styles.bubbleActive);
    }
    return (
      <TouchableOpacity style={bubbleStyle} onPress={() => this.onPress(callback, user)}>
        {!user ? (
          <Icon ios="ios-add" android="md-add" color={Colors.colorLight} />
        ) : (
          // <Text style={bubbleTextStyle}>{user.name.charAt(0)}</Text>
          <Image
            source={{ uri: user.avatar }}
            style={{ width: wp('10%'), height: wp('10%'), borderRadius: 50 }}
            resizeMode="contain"
          />
        )}
      </TouchableOpacity>
    );
  }
}

export const DOMAIN = 'http://192.168.225.41:8000';
export const MAIN_URL = 'http://192.168.225.41:8000/api/buyers';

export const URLS = {
  SYNC_SHOPPER: '/sync-shopper',
  CONNECT: '/connect/',
  CONNECTED_LIST: '/active-shopping',
  LOGIN: '/auth/login',
  OTP_VERIFY: '/auth/otp-verify',
};

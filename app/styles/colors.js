export const colorLight = '#fff';
export const colorDark = '#797979';
export const colorLightDark = '#8c8c8c';
export const colorRed = '#e76e50';
export const colorLighter = '#E8E8E8';
export const colorGreen = '#5eab63';
export const colorYellow = '#fef5a6';
export const colorApp = '#FF585D';
export const colorAppSec = '#FF585D';

export const backgroundApp = {
  backgroundColor: colorApp,
};

export const backgroundAppSec = {
  backgroundColor: colorAppSec,
};

export const backgroundWhite = {
  backgroundColor: colorLight,
};
export const backgroundGrey = {
  backgroundColor: colorLighter,
};

export const backgroundGreen = {
  backgroundColor: colorGreen,
};
export const backgroundYellow = {
  backgroundColor: colorYellow,
};

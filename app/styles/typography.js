export const fontLight = {
    fontFamily: 'Montserrat-Light',
  
}
export const fontMedium = {
    fontFamily: 'Montserrat-Medium',
  
}
export const fontBold = {
    fontFamily: 'Montserrat-Bold',
  
}
export const iosTitle = {
    fontSize : 17
  
}
export const iosParagraph = {
    fontSize : 17
}
export const iosSecondary = {
    fontSize : 15
  
}
export const iosTertiary = {
    fontSize : 13
}
export const iosSmall = {
    fontSize : 10
}
export const iosInputs = {
    fontSize : 17
}

export const androidTitle = {
    fontSize : 20
  
}
export const androidParagraph = {
    fontSize : 14
}
export const androidSecondary = {
    fontSize : 14
  
}
export const androidTertiary = {
    fontSize : 13
}

export const androidImportant = {
    fontSize : 16
}

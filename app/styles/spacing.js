export const LeftBordered = {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
}

export const RightBordered = {
    borderTopRightRadius:10,
    borderBottomRightRadius:10,
}
export const bottomBordered = {
    borderBottomRightRadius:10,
    borderBottomLeftRadius:10,
}
export const bordered = {
   borderRadius : 10
}

export const topSpace = {
   marginTop : 5
}



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { StatusBar } from 'react-native';
import { MenuProvider } from 'react-native-popup-menu';
import FlashMessage from 'react-native-flash-message';
import AppNavigator from './app/navigations/AppNavigator';

function App() {
  StatusBar.setBackgroundColor('#ff5a5f', true);

  return (
    <MenuProvider>
      <AppNavigator />
      <FlashMessage position="top" />
    </MenuProvider>
  );
}

export default App;
